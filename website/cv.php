<!DOCTYPE html>
<html lang="en" class="demo-1 no-js">

	<head>
		<?php include('includes/head.php'); ?>
	</head>
	
	<body>
	
		<section class="headerMenu">
			<h1 class="headingMenu">KOENIG Guillaume - Portfolio</h1>
		</section>
		
		<div class="container col-sm-12">

			<div class="demo-wrapper">
		
				<div class="col-sm-8">
	
					<object data="fichiers/CV_V2.pdf" type="application/pdf"  style="width:100%; height:800px;"> 
						<p style="text-align:justify;">Il semblerai que votre appareil ne dispose pas du plugin nécessaire. Vous pouvez cependant télécharger le CV <a href="resume.pdf">ici</a>. </p>  
					</object>
				 
				</div>
			
				<div class="col-sm-4" style="margin-top:10px;padding-bottom:10px;color:white;background-color:#004050;">
				
					<h2 class="Start">Curriculum Vitea</h2>
				
					<section>
						<h3>Télécharger le CV</h3>
						<p>Si vous souhaitez télécharger ce CV en français au format <i>.pdf</i>, cliquez sur ce lien : <a target="_BLANK" href="fichiers/CV_V2.pdf">CV_FR.pdf</a></p>
					</section>
					
					<section>
						<h3>Download the résumé</h3>
						<p>If you want to download my résumé, you can click here : <a target="_BLANK" href="fichiers/CVAnglais.pdf">CV_EN.pdf</a></p>
					</section>
					
				</div>
		
				<div class="col-sm-12" style="margin:30px 0;">
				</div>
				
			</div>

		</div>
		
		<div class="StatusBar">
			<?php include('includes/statusbar.php'); ?>
		</div>
		
		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
		
	</body>
</html>