<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Portfolio Guillaume KOENIG. Développeur informatique et étudiant à TélécomNancy." />
		<meta name="author" content="KOENIG Guillaume" />

		<title>KOENIG Guillaume - Portfolio</title>
		
		<link rel="icon" type="image/png" href="favicon.png" />

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,200">
		
		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		
		<!-- Custom CSS -->
		<link rel="stylesheet" type="text/css" href="css/custom.css" />
		<link rel="stylesheet" type="text/css" href="css/demo-styles.css" />

		<link rel="canonical" href="https://oldportfolio.koenigguillaume.me/" />
		
		<script>
			window.addEventListener('error', function(e) {
				document.location.href="https://oldportfolio.koenigguillaume.me";
			});
		</script>	

	</head>

	<body>
	
		<section class="headerMenu">
			<h1 class="headingMenu">KOENIG Guillaume - Portfolio</h1>
		</section>
		
		<div class="container col-sm-12">

			<div class="demo-wrapper">

				<div class="dashboard clearfix">

					<ul class="tiles">
				
						<div class="col1 clearfix">
						
							<h2 class="Start">Portfolio</h2>
						
							<a target="" href="pres.php">
								<li class="C_orange tile tile-big">
									<i class="icon fa fa-user fa-4x"></i>
									<span class="label bottom">Présentation</span> 
								</li>
							</a>
								
							<a target="" href="real.php">
								<li class="C_red tile tile-small tile tile-2">
									<i class="icon fa fa-archive fa-4x"></i>
									<span class="label bottom">Réalisations</span> 
								</li>
							</a>
			  
							<a target="" href="cv.php">
								<li class="C_steel-blue tile tile-small last tile-1">
									<i class="icon fa fa-suitcase fa-4x"></i>
									<span class="label bottom">CV</span> 
								</li>
							</a>
		
							<a target="" href="#">
								<li class="C_blue tile tile-small tile tile-2">
								<i class="icon fa fa-newspaper-o fa-4x"></i>
								<span class="label bottom">Actualités</span> 
								</li>
							</a>
						</div>

						<div class="col2 clearfix">
						
							<h2 class="Start">Réseaux</h2>

							<a target="" href="#">
								<li class="C_blue-dark tile tile-big">
									<i class="icon fa fa-facebook-square fa-4x"></i>
									<span class="label bottom">Facebook</span> 
								</li>
							</a>
								
							<a target="_blank" href="https://fr.linkedin.com/pub/guillaume-koenig/107/71a/3a6">
								<li class="C_gray tile tile-big tile tile-2">
									<i class="icon fa fa-linkedin-square fa-4x"></i>
									<span class="label bottom">LinkedIn</span> 
								</li>
							</a>
			  
							<a target="_blank" href="https://twitter.com/GKoenigTnCy">
								<li class="C_blue tile tile-small tile-1">
									<i class="icon fa fa-twitter-square fa-4x"></i>
									<span class="label bottom">Twitter</span> 
								</li>
							</a>
							
							<a href="#">
								<li class="C_red-light tile tile-small last tile tile-2">
									<i class="icon fa fa-google-plus fa-4x"></i>
									<span class="label bottom">Google +</span> 
								</li>
							</a>
					
						</div>

						<div class="col3 clearfix"> 

							<h2 class="Start">Contact</h2>						
						
							<a target="" href="mailto:guillaume.koenig@telecomnancy.net">
								<li class="C_blue tile tile-big tile-6 slideTextLeft">
									<div>
										<i class="icon fa fa-envelope-o fa-4x"></i>
										<span class="label bottom">Mail</span> 
									</div>
									<div>
										<p><span style="color:white;">guillaume.koenig@telecomnancy.net</span></p>
									</div>
								</li>
							</a>
							
							<a target="" href="tel:+33631459636">
								<li class="C_green tile tile-big tile-6 slideTextLeft">
									<div>
										<i class="icon fa fa-mobile fa-5x"></i>
										<span class="label bottom">Téléphone</span> 
									</div>
									<div>
										<p><span style="color:white;">+&nbsp;(33)&nbsp;631459636</span></p>
									</div>
								</li>
							</a>
							
						</div>
						
						<div class="col4 clearfix">
						
							<h2 class="Start">Accès&nbsp;rapide</h2>
							
							<a target="" href="#">
								<li class="C_red-light tile tile-small tile-1">
									<i class="icon fa fa-check-circle-o fa-3x"></i>
									<span class="label bottom">TNCY Dépot</span> 
								</li>
							</a>
							
							<a target="" href="#">
								<li class="C_rod tile tile-small last tile tile-2">
									<i class="icon fa fa-check-circle-o fa-3x"></i>
									<span class="label bottom">AASimserhof</span> 
								</li>
							</a>
					
						</div>
						
					</ul>
				
				</div>

			</div>

			<div class="StatusBar">
				<?php include('includes/statusbar.php'); ?>
			</div>
			
		</div>

	</body>
	
</html>
