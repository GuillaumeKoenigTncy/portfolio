<?php
 
  /**
   * Constantes d'accès à la base de données
   * et de configuration du livre d'or
   **/
 
  // Adresse du serveur de base de données
  define('DB_SERVEUR', 'localhost');
  
  // Login
  define('DB_LOGIN','admin_portfolio');
 
  // Mot de passe
  define('DB_PASSWORD','SMm2thWw6yWrqFL8');
 
  // Nom de la base de données
  define('DB_NOM','BDD_portfolio');
 
  // Nom de la table du livre d'or
  define('DB_GUESTBOOK_TABLE','actus');
 
  // Driver correspondant à la BDD utilisée
  define('DB_DSN','mysql:host='. DB_SERVEUR .';dbname='. DB_NOM);
 
  // Nombre de messages à afficher par page
  define('MAX_MESSAGES_PAR_PAGE', 10);
  
  // URL du livre d'or
  define('URL_GUESTBOOK', 'actu.php');
  ?>