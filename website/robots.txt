User-agent: *
Allow: /
Disallow: /*?*
Disallow: /*admin*
Disallow: /*fichiers*
Disallow: /*files*
Disallow: /admin
Disallow: /fichiers
Disallow: /files

Sitemap: https://oldportfolio.koenigguillaume.me/sitemap.xml