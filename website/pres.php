﻿<!DOCTYPE html>
<html lang="fr" class="demo-1 no-js">

	<head>
		<?php include('includes/head.php'); ?>
	</head>
	
	<body>
	
		<section class="headerMenu">
			<h1 class="headingMenu">KOENIG Guillaume - Portfolio</h1>
		</section>
		
		<div class="container col-sm-12">

			<div class="demo-wrapper">
				
				<div class="col-sm-12" style="margin-top:10px;padding-bottom:10px;color:white;background-color:#004050;">
					
					<section>	
						
						<article>
							<h2 style="text-align:center;" class="Start">Bonjour à tous</h2>
							<p style="font-size:18px;text-align:center;">Mon nom est Guillaume KOENIG. Je suis actuellement étudiant en ingénierie informatique à Télécom Nancy.</p>
							<br/>
							<p style="font-size:18px;text-align:center;">Vous trouverez sur ce portfolio toutes sortes d'informations à mon sujet et notamment les différents projets réalisés durant mes deux années de DUT «&nbsp;Informatique&nbsp;» ou ma formation d'ingénieur à Télécom Nancy, mais aussi ceux réalisés personnellement..</p>
						</article>
						
					</section>
				
				</div>
				
				<div class="col-sm-12" style="margin-top:10px;padding-bottom:10px;color:white;background-color:#004050;">
					
					<section>	
						
						<article>
							<h2 class="Start">Présentation</h2>
							
							<p style="font-size:18px;text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Actuellement étudiant en ingénierie informatique, j'ai décidé de choisir ce domaine de formation car depuis ma prime jeunesse je suis passionné par l'informatique et par la bidouille. L'informatique, sous toutes ses composantes, occupe une place prépondérante dans la société et ne cesse de se renforcer. Ce domaine me semble une voie professionnelle royale. En effet, l'informatique devient de jour en jour plus omniprésente dans notre vie quotidienne.
							</p>

							<hr/>
							
							<p style="font-size:18px;text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Mon BAC Scientifique - Science de l'ingénieur - m'a permis de conforter mon choix d'étudier l'informatique. En effet, dans les divers projets réalisés en classe de première et de terminale, j'ai généralement effectué les tâches de programmations. D'abord en C sur Arduino Uno puis en C# sur Fez Panda II. Arduino et Fez circuits imprimés en matériel libre composé d'un microcontrôleur permettant d'effectuer des tâches plus ou moins complexes.  
								<br/>
								Le premier projet consistait, via un accéléromètre, à maintenir l'assiette d'un avion miniature à l'horizontale en cas de perte de signal de la télécommande. Le second projet, réalisé en classe de terminale, consistait à automatiser la distribution de médicaments. Pour ce projet, il a fallu créer "l'armoire à pharmacie" et le programme commandant celle-ci grâce à une Fez Panda II.
							</p>

							<hr/>
							
							<p style="font-size:18px;text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; A la suite de mon baccalauréat, j'ai naturellement décidé de me tourner vers un IUT proposant une formation en informatique. J'ai donc choisi l'IUT de Metz. Cette formation m'a permis d'acquérir de nombreuses connaissances dans divers domaines, aussi bien informatiques que non informatiques. Les nombreux projets proposés ont permis de mettre en œuvre de façon concrète les différents aspects théoriques vus lors des différents cours magistraux.
							</p>
							
							<hr/>
							
							<p style="font-size:18px;text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Cette formation professionnalisante et riche en enseignements c'est conclue par un stage de 10 semaines en entreprise. J'ai effectué ce stage de clôture au SYDEME, il s'agit du Syndicat Mixte de Transport et de Traitement des Déchets Ménagers de Moselle-Est. Le SYDEME dispose d'un pôle "éducation" qui organise des opérations sur les différents sites du syndicat. Les personnels de ce pôle ont souhaité disposer d'un outil de gestion d'animation de type quizz, celui-ci permettant de vérifier les connaissances des participants et ainsi d'adapter la stratégie de communication. L'outil dispose aussi d'une puissante partie statistique permettant, là aussi, de gérer la stratégie du pôle.
							</p>
						</article>
						
					</section>
				
				</div>
				
				<div class="col-sm-12" style="margin:30px 0;">
				</div>
				
			</div>

		</div>
		
		<div class="StatusBar">
			<?php include('includes/statusbar.php'); ?>
		</div>
		
		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
		
	</body>
</html>