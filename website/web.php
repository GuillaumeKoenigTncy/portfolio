﻿<style>
	<!-- Media queries A GARDER -->
	@media screen and (max-width: 480px) {
	.divNCM {
	display: none;
	}
	}
	
	@media screen and (min-width: 480px) {
	.divNCP {
	display: none;
	}
	}
	
	@media screen and (max-width: 480px) {
	.divOCM {
	display: inline;
	}
	}
	
	@media screen and (min-width: 480px) {
	.divOCP {
	display: inline;
	}
	}	
	<!-- FIN Media queries -->
	
	.leg{
	background-color: #ffffff;
    border: 1px solid black;
    opacity: 0.6;
	margin-left:15px;color:#004050;font-size:18px;
	}
</style>

<ul class="col-sm-9">
	
	<div class="clearfix">
		
		<h2 class="Start">Web</h2>
		
		<div class="col-sm-12" style="background-color:#004050;color:white;font-size:14px;text-align:justify;">
			
			<div id="page-top" class="index">
				<!-- Portfolio Grid Section -->
				<section id="portfolio">
					<div class="col-lg-12 text-center">
						<h2>&nbsp;</h2>
					</div>
					
					<div class="row">
						<div class="col-sm-4 portfolio-item">
							<a href="#portfolioModal8" class="portfolio-link" data-toggle="modal">
								<div class="caption">
									<div class="caption-content">
										<i class="fa fa-search-plus fa-3x"></i>
									</div>
								</div>
								<img class="img-responsive" alt="Stage Sydeme Quizz" src="images/AAShome.png" style="margin-left: auto; margin-right: auto;"/>
								<span class="leg label bottom" ><b>AAS</b></span> 
							</a>
						</div>	
						<div class="col-sm-4 portfolio-item">
							<a href="#portfolioModal7" class="portfolio-link" data-toggle="modal">
								<div class="caption">
									<div class="caption-content">
										<i class="fa fa-search-plus fa-3x"></i>
									</div>
								</div>
								<img class="img-responsive" alt="MyCMS" src="images/TNCYdepot.png" style="margin-left: auto; margin-right: auto;"/>
								<span class="leg label bottom" ><b>TNCY Dépot</b></span> 
							</a>
						</div>	
						<div class="col-sm-4 portfolio-item">
							<a href="#portfolioModal6" class="portfolio-link" data-toggle="modal">
								<div class="caption">
									<div class="caption-content">
										<i class="fa fa-search-plus fa-3x"></i>
									</div>
								</div>
								<img class="img-responsive" alt="Stage Sydeme Quizz" src="images/syd1.png" style="margin-left: auto; margin-right: auto;"/>
								<span class="leg label bottom" ><b>Sydeme Quizz</b></span> 
							</a>
						</div>	
					</div>
					
					
					<div class="row">
						<div class="col-sm-4 portfolio-item">
							<a href="#portfolioModal4" class="portfolio-link" data-toggle="modal">
								<div class="caption">
									<div class="caption-content">
										<i class="fa fa-search-plus fa-3x"></i>
									</div>
								</div>
								<img class="img-responsive" alt="MyCMS" src="images/cms.png" style="margin-left: auto; margin-right: auto;"/>
								<span class="leg label bottom" ><b>MyCMS</b></span> 
							</a>
						</div>	
						<div class="col-sm-4 portfolio-item">
							<a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
								<div class="caption">
									<div class="caption-content">
										<i class="fa fa-search-plus fa-3x"></i>
									</div>
								</div>
								<img class="img-responsive" alt="portfolio v1" src="images/portfolio1.png" style="margin-left: auto; margin-right: auto;"/>
								<span class="leg label bottom" ><b>Portfolio V1</b></span> 
							</a>
						</div>
						<div class="col-sm-4 portfolio-item" >
							<a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
								<div class="caption">
									<div class="caption-content">
										<i class="fa fa-search-plus fa-3x"></i>
									</div>
								</div>
								<img class="img-responsive" alt="Simserhof" src="images/sitesims.png" style="margin-left: auto; margin-right: auto;"/>
								<span class="leg label bottom" ><b>Simserhof</b></span> 
							</a>
						</div>
					</div>
					
					<div class="row">
						<div class="col-sm-4 portfolio-item">
							<a href="#portfolioModal3" class="portfolio-link" data-toggle="modal">
								<div class="caption">
									<div class="caption-content">
										<i class="fa fa-search-plus fa-3x"></i>
										</div>
								</div>
								<img class="img-responsive" alt="Trombinoscope" src="images/trombi.png" style="margin-left: auto; margin-right: auto;"/>
								<span class="leg label bottom" ><b>Trombinoscope</b></span> 
							</a>
						</div>
						<div class="col-sm-4 portfolio-item">
							<a href="#portfolioModal2" class="portfolio-link" data-toggle="modal">
								<div class="caption">
									<div class="caption-content">
										<i class="fa fa-search-plus fa-3x"></i>
									</div>
								</div>
								<img class="img-responsive" alt="Site IUT" src="images/siteiut2.png" style="margin-left: auto; margin-right: auto;"/>
								<span class="leg label bottom" ><b>L'info de l'info</b></span> 
							</a>
						</div>
					</div>
					
				</section>
				
				<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
				<div class="scroll-top page-scroll visible-xs visible-sm">
					<a class="btn btn-primary" href="#page-top">
						<i class="fa fa-chevron-up"></i>
					</a>
				</div>
				
				<!-- Portfolio Modals -->
				
				
				<div class="portfolio-modal modal fade" id="portfolioModal8" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-content">
						<div class="close-modal" data-dismiss="modal">
							<div class="lr">
								<div class="rl">
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-lg-10 col-lg-offset-1">
									<div class="modal-body">							
										<section class="section_Modal">
											
											<h2 class="text-center">AAS - Association des Amis du Simserhof </h2>
											
											<br />
											<br />
											
											<div class="col-lg-4">
											</div>
											<div class="col-lg-4">
												<img class="img-responsive" alt="AAS" src="images/AAShome.png" style="float:left; margin:5px;"/>
											</div>
											<div class="col-lg-4">
											</div>
											
											<div class="col-lg-12" style="margin-top:50px;">
												<p>Le Simserhof est un ouvrage de la fameuse ligne Maginot, construit entre 1929 et 1938 ce fort est visité chaque année par plus de 40.000 personnes.
												Le fort devrai se doter d'une association dans le but de poursuivre les travaux de mise en valeur. Cette association s'occupera nottement des parties avants abritant les blocs de combats. 
												Afin de faciliter la communication de la future assocaition, un site web a été réalisé, de même qu'une page Facebook.
												</p>	
											</div>
											
											<div class="col-lg-12">
												<p>
													Langages utilisés : <span class="uses">HTML5, CSS3, PHP et JavaScript</span>
													<br/>
													Méthodes utilisées : <span class="uses">WYSIWYG, PDO & MySQL, JavaScript, JQuery, etc. </span>
													<br/>
													Date : <span class="uses">15 Juin 2016 - 15 Juillet 2016</span>
												</p>
											</div>
											
											<div class="col-lg-12">
												<p>Vous trouverez le site web à cette adresse : AAS - Association des Amis du Simserhof. 
												</p>
											</div>
											
										</section>
										
										<div class="col-lg-12" style="margin-top:50px;">
											<button type="button" class="btn btn-default" data-dismiss="modal"> &nbsp;&nbsp;  Fermer &nbsp;&nbsp;  </button>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
				
				<div class="portfolio-modal modal fade" id="portfolioModal7" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-content">
						<div class="close-modal" data-dismiss="modal">
							<div class="lr">
								<div class="rl">
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-lg-10 col-lg-offset-1">
									<div class="modal-body">							
										<section class="section_Modal">
											
											<h2 class="text-center">Dépot de cours TNCY </h2>
											
											<br />
											<br />
											
											<div class="col-lg-4">
											</div>
											<div class="col-lg-4">
												<img class="img-responsive" alt="AAS" src="images/TNCYdepot.png" style="float:left; margin:5px;"/>
											</div>
											<div class="col-lg-4">
											</div>
											
											<div class="col-lg-12" style="margin-top:50px;">
												<p> Afin de permettre au plus grand nombre de profiter des cours de Télécom Nancy, j'ai créé ce dépot personnel. Il permet de partager mes cours au plus grand nombre. 
												Il référence les cours de la première année et permet de profiter aussi bien des cours magistraux que des TD et TP.	En plus des cours, des anciens examens sont disponibles pour s'entrainer avant les partiels.
												
												</p>	
											</div>
											
											<div class="col-lg-12">
												<p>
													Langages utilisés : <span class="uses">HTML5, CSS3, PHP </span>
													<br/>
													Méthodes utilisées : <span class="uses"> \ </span>
													<br/>
													Date : <span class="uses">01 Septembre 2016 - 31 Juillet 2016</span>
												</p>
											</div>
											
											<div class="col-lg-12">
												<p>Vous trouverez le site web sur ce serveur à cette adresse : Télécom Nancy Dépot. 
												</p>
											</div>
											
										</section>
										
										<div class="col-lg-12" style="margin-top:50px;">
											<button type="button" class="btn btn-default" data-dismiss="modal"> &nbsp;&nbsp;  Fermer &nbsp;&nbsp;  </button>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-content">
						<div class="close-modal" data-dismiss="modal">
							<div class="lr">
								<div class="rl">
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-lg-10 col-lg-offset-1">
									<div class="modal-body">
										<section class="section_Modal">
											
											<h2 class="text-center">Site Web du Simserhof</h2>
											
											<br />
											<br />
											
											<div class="col-lg-4">
											</div>
											<div class="col-lg-4">
												<img alt="Simserhof" src="images/sitesims.png" style="float:left; margin:5px;"/>
											</div>
											<div class="col-lg-4">
											</div>
											
											<div class="col-lg-12" style="margin-top:50px;">
												<p>Le Simserhof est un ouvrage de la Ligne Maginot se situant à Bitche en Moselle. L'ancien site web datant de 2002 était trop vétuste et ne correspondait plus au besoin du fort qui avait un besoin de pouvoir modifier rapidement des informations présentes sur le site. Il a aussi fallu intégrer un système d'actualité et de livre d'or afin de pouvoir communiquer rapidement avec les éventuelles visiteurs. J'ai donc créer un mini-CMS permettant de gérer rapidement les site web sans trop de difficulté. Le site se compose de deux parties, un front-office accessible uniquement aux visiteurs et un back-office pour l'administration.
												</p>
											</div>
											
											<div class="col-lg-12">
												<p>
													Langages utilisés : <span class="uses">HTML5, CSS3, PHP et JavaScript</span>
													<br/>
													Méthodes utilisées : <span class="uses">PDO & MySQL, gestion de session</span>
													<br/>
													Date : <span class="uses">Septembre 2014 / Décembre 2014</span>
												</p>
											</div>
											
											<div class="col-lg-12">
												<p style="color:red;"> Le site web n'est plus accessible depuis la réinstallation du serveur car il n'est pas considéré comme pertinent. 
												Le site n'étant pas utilisé et des failles de sécurité ayant été découvertes il a été retiré du serveur.
												</p>
											</div>
											
										</section>
										
										<div class="col-lg-12" style="margin-top:50px;">
											<button type="button" class="btn btn-default" data-dismiss="modal"> &nbsp;&nbsp;  Fermer &nbsp;&nbsp;  </button>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-content">
						<div class="close-modal" data-dismiss="modal">
							<div class="lr">
								<div class="rl">
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-lg-10 col-lg-offset-1">
									<div class="modal-body">
										<section class="section_Modal">
											
											<h2 class="text-center">L'info des infos</h2>
											
											<br />
											<br />
											
											<div class="col-lg-4">
											</div>
											<div class="col-lg-4">
												<img alt="Site IUT" src="images/siteiut.png" style="float:right; margin:5px;"/>
											</div>
											<div class="col-lg-4">
											</div>
											
											<div class="col-lg-12" style="margin-top:50px;">
												<p>Lors de la première année de DUT, les étudiants doivent réaliser un site web totalement fonctionnel en n'utilisant que les langages HTML5 et CSS3. Pour ce faire, les étudiants se mettent en groupe de 5. Ce site Web à pour but de présenter rapidement l'IUT aux nouveaux étudiants. Il permet de s'informer simplement sur les bons petit coins, les bons plans, et les infos pratiques du département. Ce site (la page d'acceuil seulement) est aussi traduit en anglais ce qui permet aussi aux étudiants étangers de pouvoir s'informer.
												</p>	
											</div>
											
											<div class="col-lg-12">
												<p>
													Langages utilisés : <span class="uses">HTML5, CSS3</span>
													<br/>
													Méthodes utilisées : <span class="uses"> / </span>
													<br/>
													Date : <span class="uses">Octobre et Novembre 2013</span>
												</p>
											</div>
											
											<div class="col-lg-12">
												<p>Vous trouverez le site web sur ce serveur à cette adresse : Présentaion du Dpt INFO
												</p>
											</div>
											
										</section>
										
										<div class="col-lg-12" style="margin-top:50px;">
											<button type="button" class="btn btn-default" data-dismiss="modal"> &nbsp;&nbsp;  Fermer &nbsp;&nbsp;  </button>
										</div>		
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-content">
						<div class="close-modal" data-dismiss="modal">
							<div class="lr">
								<div class="rl">
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-lg-10 col-lg-offset-1">
									<div class="modal-body">
										<section class="section_Modal">
											
											<h2 class="text-center">Site Web de gestion de trombinoscope</h2>
											
											<br />
											<br />
											
											<div class="col-lg-4">
											</div>
											<div class="col-lg-4">
												<img class="img-responsive" alt="Trombinoscope" src="images/trombi.png" style="float:left; margin:5px;"/>
											</div>
											<div class="col-lg-4">
											</div>
											
											<div class="col-lg-12" style="margin-top:50px;">
												<p>Afin de poursuivre les travaux effectués pendant la première année, la deuxième année permet aux étudiants de compléter leurs connaissances en programmation Web par l'apprentissage du PHP5, du JavaScript et de Ajax. Pour mettre en oeuvre ces nouveaux langages, il nous a était demandé de réaliser un trombinoscope en ligne. J'ai réalisé ce site avec <a href="https://regishergott.fr/" target="_blank"> HERGOTT Régis </a>, un collègue de ma promotion.
												</p>	
											</div>
											
											<div class="col-lg-12">
												<p>
													Langages utilisés : <span class="uses">HTML5, CSS3, PHP, JavaScript et AJAX</span>
													<br/>
													Méthodes utilisées : <span class="uses">PDO & MySQL</span>
													<br/>
													Date : <span class="uses">Octobre et Novembre 2014</span>
												</p>
											</div>
											
											<div class="col-lg-12">
												<p style="color:red;"> Le site web n'est plus accessible depuis la réinstallation du serveur car il n'est pas considéré comme pertinent.
												</p>
												<!--<p> Ce site nécessitant une connexion sécurisée, voici les couples <i>login</i>/<i>password</i> afin de l'utiliser :
													<br />
													&nbsp;&nbsp;&nbsp; - Accès étudiant : koegui / koko
													<br />
													&nbsp;&nbsp;&nbsp; - Accès enseignant : admin / admin
												</p>-->
											</div>
											
											<div class="col-lg-12">
												<!--<p>Vous trouverez le site web sur ce serveur à cette adresse : <a href="../trombinoscope/index.php" target="_blank"> Gestion de trombinoscope</a>. Et le sujet à cette adresse ci : <a href="fichiers/projetweb2014_15.pdf" target="_blank"> Sujet Web 2014 - 2015 </a> 
												</p>-->
											</div>
										</section>
										
										<div class="col-lg-12" style="margin-top:50px;">
											<button type="button" class="btn btn-default" data-dismiss="modal"> &nbsp;&nbsp;  Fermer &nbsp;&nbsp;  </button>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-content">
						<div class="close-modal" data-dismiss="modal">
							<div class="lr">
								<div class="rl">
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-lg-10 col-lg-offset-1">
									<div class="modal-body">
										<section class="section_Modal">
											
											<h2 class="text-center">Projet Tutoré : un CMS</h2>
											
											<br />
											<br />
											
											<div class="col-lg-4">
												
											</div>
											<div class="col-lg-4">
												<img alt="Simserhof" src="images/cms.png" class="img-responsive" style="float:left; margin:5px;"/>
											</div>
											<div class="col-lg-4">
												
											</div>
											
											<div class="col-lg-12" style="margin-top:50px;">
												<p>Pour valider le Semestre 3, les étudiants doivent réaliser un projet de synthèse par groupe de 5. Cette années, le sujet de mon groupe consistait à gréer un CMS, c'est-à-dire, un site web totalement paramètrable par son utilisateur. Ce site Web est totalement paramètrable, il est possible de changer le placement des éléments (module d'information et menu), de faire disparaitres des éléments (en-tête et pied-de-page) et de modifier les couleurs des titres, textes et fonds. Ce site est géré grâce à une partie d'administration sécurisée. 
												</p>	
											</div>
											
											<div class="col-lg-12">
												<p>
													Langages utilisés : <span class="uses">HTML5, CSS3</span>
													<br/>
													Méthodes utilisées : <span class="uses"> XML </span>
													<br/>
													Date : <span class="uses">Novembre 2014 - Janvier 2015</span>
												</p>
											</div>
											
											<div class="col-lg-12">
												<p> Ce sitte nécessitant une connexion sécurisée pour son administration, veuillez me contacter afin de les récupérer.
												</p>
											</div>
											
											<div class="col-lg-12">
												<p>Vous trouverez le site web sur ce serveur à cette adresse : My CMS. Et son administration ici : Administration My CMS. Enfin le sujet à cette adresse ci : <a href="fichiers/projettut2014_15.pdf" target="_blank"> Sujet Synthèse 2014 - 2015 </a> 
												</p>
											</div>				
										</section>
										
										<div class="col-lg-12" style="margin-top:50px;">
											<button type="button" class="btn btn-default" data-dismiss="modal"> &nbsp;&nbsp;  Fermer &nbsp;&nbsp;  </button>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-content">
						<div class="close-modal" data-dismiss="modal">
							<div class="lr">
								<div class="rl">
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-lg-10 col-lg-offset-1">
									<div class="modal-body">							
										<section class="section_Modal">
											
											<h2 class="text-center">Site Web <i>Portfolio</i> </h2>
											
											<br />
											<br />
											
											<div class="col-lg-4">
												
											</div>
											<div class="col-lg-4">
												<img alt="Portfolio CMS" src="images/portfolio1.png" class="img-responsive" style="float:left; margin:5px;"/>
											</div>
											<div class="col-lg-4">
												
											</div>
											
											<div class="col-lg-12" style="margin-top:50px;">
												<p> Dans le cadre le l'UE PPP (Projet Personnel et Professionnel) du 3ème semestre du DUT il nous est demandé de réaliser un petit portfolio permettant de présenter rapidement les différents projets effectués pendant les deux années de DUT mais aussi le reste de l'année à titre personnel
												</p>	
											</div>
											
											<div class="col-lg-12">
												<p>
													Langages utilisés : <span class="uses">HTML5, CSS3, PHP, </span>
													<br/>
													Méthodes utilisées : <span class="uses"> / </span>
													<br/>
													Date : <span class="uses">Décembre 2014</span>
												</p>
											</div>
											
											<div class="col-lg-12">
												<p> Vous trouverez le portfolio sur ce serveur à cette adresse : Portfolio V1.0.
												</p>
											</div>					
										</section>
										
										<div class="col-lg-12" style="margin-top:50px;">
											<button type="button" class="btn btn-default" data-dismiss="modal"> &nbsp;&nbsp;  Fermer &nbsp;&nbsp;  </button>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-content">
						<div class="close-modal" data-dismiss="modal">
							<div class="lr">
								<div class="rl">
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-lg-10 col-lg-offset-1">
									<div class="modal-body">							
										<section class="section_Modal">
											
											<h2 class="text-center">Gestion d'animation - SYDEME </h2>
											
											<br />
											<br />
											
											<div class="col-lg-4">
												<img class="img-responsive" alt="Trombinoscope" src="images/syd1.png" style="float:left; margin:5px;"/>
											</div>
											<div class="col-lg-4">
												<img class="img-responsive" alt="Trombinoscope" src="images/syd2.png" style="float:left; margin:5px;"/>
											</div>
											<div class="col-lg-4">
												<img class="img-responsive" alt="Trombinoscope" src="images/syd3.png" style="float:left; margin:5px;"/>
											</div>
											
											<div class="col-lg-12" style="margin-top:50px;">
												<p> Afin de cloturer notre formation de DUT, chaque étudiant est dans l'obligation de réaliser un stage en entreprise d'une durée de 10 semaines. J'ai réalisé ce stage au SYDEME, le Syndicat Mixte des Traitement des Déchets de Moselle Est. Il effectue des opérations de communication dans le but de sensibiliser le public aux thématiques de gestion des déchets.
												</p>	
												<p> Parmis ces publics, de nombreuses écoles sont invité au SYDEME pour y effectuer des activités. Le service communication souhaiterai savoir ce que les enfants ont retenu au terme de leur venu. Pour ce faire, j'ai du développer une application web permettant de gérer des animations du type quizz. Cette application permet la création, la modification et la suppression d'un quizz mais elle permet aussi d'effectuer de nombreuses statistiques concernant les visiteurs.													
												</p>
											</div>
											
											<div class="col-lg-12">
												<p>
													Langages utilisés : <span class="uses">HTML5, CSS3, PHP </span>
													<br/>
													Méthodes utilisées : <span class="uses"> PDO & MySQL, JavaScript, JQuery, etc. </span>
													<br/>
													Date : <span class="uses">07 Avril 2015 - 12 Juin 2015</span>
												</p>
											</div>
											
											<div class="col-lg-12">
												<p> Ce site nécessitant une connexion sécurisée, voici les couples <i>login</i>/<i>password</i> afin de l'utiliser en tant qu'administrateur, pour des raisons de sécurité le compte est restreint (il ne peu qu'afficher les informations):
													<br />
													&nbsp;&nbsp;&nbsp; - Accès administrateur : userA / userA
												</p>
											</div>
											
											<div class="col-lg-12">
												<p>Vous trouverez le site web sur ce serveur à cette adresse : Sydeme Quizz. 
												</p>
											</div>
											
										</section>
										
										<div class="col-lg-12" style="margin-top:50px;">
											<button type="button" class="btn btn-default" data-dismiss="modal"> &nbsp;&nbsp;  Fermer &nbsp;&nbsp;  </button>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- jQuery -->
				<script src="js/jquery.js"></script>
				
				<!-- Bootstrap Core JavaScript -->
				<script src="js/bootstrap.min.js"></script>
				
				<!-- Plugin JavaScript -->
				<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
				<script src="js/classie.js"></script>
				<script src="js/cbpAnimatedHeader.js"></script>
				
				<!-- Contact Form JavaScript -->
				<script src="js/jqBootstrapValidation.js"></script>
				<script src="js/contact_me.js"></script>
				
				<!-- Custom Theme JavaScript -->
				<script src="js/freelancer.js"></script>
				
			</div>
			
		</div>
		
	</div>
	
</ul>

<style>
	.leg{
	color : black;
	background-color: #ffffff;
    border: 1px solid black;
    opacity: 0.8;
	margin-left:15px;
	font-size:16px;
	}
</style>