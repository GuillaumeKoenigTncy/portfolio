<!DOCTYPE html>
<html lang="fr" class="demo-1 no-js">

	<head>
		<?php include('includes/head.php'); ?>
	</head>
	
	<body>
	
		<section class="headerMenu">
			<h1 class="headingMenu">KOENIG Guillaume - Portfolio</h1>
		</section>
		
		<div class="container col-sm-12">

			<div class="demo-wrapper">
			
				<ul class="col-sm-3">
				
					<div class="clearfix">
						
						<h2 class="Start">Réalisations</h2>
					
						<a target="" href="?real=web">
							<li class="C_orange tile tile-big">
								<img style="height:64px;width:64px;" src="images/logo/html3.png" alt="HTML" class="icon">
								<span class="label bottom">Projets Web</span> 
							</li>
						</a>
							
						<a target="" href="?real=java">
							<li class="C_red tile tile-big">
								<img style="height:64px;width:64px;" src="images/logo/logotype49.png" alt="HTML" class="icon">
								<span class="label bottom">Projets Java</span> 
							</li>
						</a>
		  
						<a target="" href="?real=c">
							<li class="C_steel-blue tile tile-big">
								<img style="height:64px;width:64px;" src="images/logo/programming2.png" alt="HTML" class="icon">
								<span class="label bottom">Projets C</span> 
							</li>
						</a>
						
						<a target="" href="?real=autre">
							<li class="C_blue tile tile-big">
								<img style="height:64px;width:64px;" src="images/logo/screen125.png" alt="HTML" class="icon">
								<span class="label bottom">Autres projets</span> 
							</li>
						</a>
						
					</div>
				
				</ul>
				
				<?php
					if (isset ($_GET['real'])){
						$page = $_GET['real'];
						if(isset($page) && file_exists("$page.php")){
							include("$page.php");
						}else{
							include("realD.php");
						}	
					}else{
						include("realD.php");
					}
				?>
				
				<div class="col-sm-12" style="margin:30px 0;">
				</div>
				
			</div>

		</div>
		
		<div class="StatusBar">
			<?php include('includes/statusbar.php'); ?>
		</div>
		
		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
		
	</body>
</html>