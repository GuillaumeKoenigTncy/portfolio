<div class="col-sm-9">

	<div class="clearfix">

		<h2 class="Start">Accueil</h2>

		<div class="col-sm-12" style="padding:10px 0;background-color:#004050;color:white;font-size:14px;text-align:justify;">
		
			<div class="col-sm-12">
				<p>
					Dans cette rubrique vous trouverez sur la gauche les différents thèmes couvert par mes réalisations. 
				</p>
			</div>
		
			<div class="col-sm-12">
				<h3>Résumé des projets IUT</h3>
				<p>
					Vous pouvez télécharger ici un .pdf qui synthétise les projets du DUT : <a href="fichiers/Projets2A.pdf" target="_blank"> projets2A.pdf </a>
				</p>
				<p>
					Tous les projets ne sont pas forcément disponibles sur ce portfolio, certains ont été perdus et d'autres ne sont pas informatique. De même, certains projet ne sont pas référencés sur cetta feuille de rappel.
				</p>
			</div>
			
		</div>
		
	</div>
	
</div> 