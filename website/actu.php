<?php

	/**
	 * Programme principal
	 * Construit la page à partir de tous les fichiers
	 */
	require(dirname(__FILE__).'/actus/actus-config.inc.php');
	require(dirname(__FILE__).'/actus/actus-model.inc.php');
	require(dirname(__FILE__).'/actus/actus-controller.inc.php');
	
?>

<!DOCTYPE html>
<html lang="fr" class="demo-1 no-js">

	<head>
		<?php include('includes/head.php'); ?>
		<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

	<style>
		br{
			display:none;
		}
	</style>

	</head>

	<body>
	
		<section class="headerMenu">
			<h1 class="headingMenu">KOENIG Guillaume - Portfolio</h1>
		</section>
		
		<div class="container col-sm-12">

			<div class="demo-wrapper">
			
				<div class="col-sm-12">

					<div class="clearfix">

						<h2 class="Start">Actualités <small style="color:white;">(<?php echo $iNombreDeMessages; ?> disponible<?php if ($iNombreDeMessages > 1) : ?>s<?php endif; ?>)</small></h2>
						

						<div class="col-sm-12" style="background-color:#004050;color:white;font-size:14px;text-align:justify;">
				
							
							<?php 
								if($_COOKIE['connect'] == "OK"){
							?>
							<div class="col-sm-12" style="margin-bottom:15px;">
								<h2>Nouvelle actualité :</h2>
					 
								<?php /** Affichage des erreurs générées **/ ?>
								<?php if (sizeof($aListeErreurs) > 0) : ?>
								<ul>
								  <?php foreach ($aListeErreurs as $sErreur) : ?>
								  <li><?php echo htmlspecialchars($sErreur); ?></li>
								  <?php endforeach; ?>
								</ul>
								<?php endif; ?>
					 
								<form action="" method="post" style="background-color:#004050;">
									<div>
										<textarea name="message" id="message" rows="10" cols="45"><?php if (!empty($_POST['message'])) : echo htmlspecialchars($_POST['message']); endif; ?></textarea>
									</div>
									<div>
										<br/>
										<input style="margin:10px; 0 0 20px; color:black;" class="btn btn-info" type="submit" name="envoyer" value="Soumettre" />
									</div>
								</form>
								<hr />
							</div>
							<?php 
								}
							?>

						
							<div class="col-sm-12" style="margin-top:25px;">
					 
								<?php /** Affichage des messages **/ ?>
								<?php if ($iNombreDeMessages > 0) { ?>
					
								<?php foreach ($aListeMessages as $oMessage){ ?>
									<div>
										<p>
											Le <?php echo convertirDate($oMessage->date_actu); ?>
											<?php 
											if($_COOKIE['connect'] == "OK"){
											?>
											<a style="margin-left:25px;" href="?delete=<?php echo $oMessage->id_actu; ?>">
												<img border="0" alt="delete" src="images/logo/delete.png" width="24" height="24">
											</a>
											<?php } ?>
										</p>
										<blockquote>
											
												<?php echo nl2br($oMessage->actu); ?>
											
										</blockquote>
									</div>
									<hr/>
								<?php } ?>
								
								<?php /** Affichage de la pagination si nécessaire **/ ?>
								<?php if ($iNombreDeMessages > MAX_MESSAGES_PAR_PAGE) { ?>
								<big>
								<div class="pagination">
									<?php echo paginer($iNombreDeMessages, MAX_MESSAGES_PAR_PAGE, $iNumeroDePageCourante, 4, 4, 1, 1); ?>
								</div>
								</big>
								<?php } ?>
								<?php }else{ ?>
								<p>
									Aucun message enregistré
								</p>
								<?php } ?>
				
							</div>
						
						</div>
						
					</div>
					
				</div> 
				
								
				<div class="col-sm-12" style="margin:30px 0;">
				</div>
			
			</div>
			
		</div>
		
		<div class="StatusBar">
			<?php include('includes/statusbar.php'); ?>
		</div>
		

		<script type="text/javascript">
			CKEDITOR.replace( 'message' );
		</script>

		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
		
	</body>
</html>