﻿<style>
	@media screen and (max-width: 480px) {
	  .divNCM {
		display: none;
	  }
	}
		
	@media screen and (min-width: 480px) {
	  .divNCP {
		display: none;
	  }
	}
		
	@media screen and (max-width: 480px) {
	  .divOCM {
		display: inline;
	  }
	}
		
	@media screen and (min-width: 480px) {
	  .divOCP {
		display: inline;
	  }
	}	
</style>

<ul class="col-sm-9">

	<div class="clearfix">

		<h2 class="Start">Java</h2>

		<div class="col-sm-12" style="background-color:#004050;color:white;font-size:14px;text-align:justify;">
		
			<div id="page-top" class="index">
				<!-- Portfolio Grid Section -->
				<section id="portfolio">
					<div class="col-lg-12 text-center">
						<h2>&nbsp;</h2>
					</div>
					<div class="row">
						<div class="col-sm-4 portfolio-item">
							<a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
								<div class="caption">
									<div class="caption-content">
										<i class="fa fa-search-plus fa-3x"></i>
									</div>
								</div>
								<img src="images/Presentation/JAVA.jpg" class="img-responsive" alt="">
								<span class="leg label bottom" ><b>Dessin assisté</b></span> 
							</a>
						</div>
						<div class="col-sm-4 portfolio-item">
							<a href="#portfolioModal2" class="portfolio-link" data-toggle="modal">
								<div class="caption">
									<div class="caption-content">
										<i class="fa fa-search-plus fa-3x"></i>
									</div>
								</div>
								<img src="images/AEDI1.png" class="img-responsive" alt="AEDI">
								<span class="leg label bottom" ><b>AEDI</b></span> 
							</a>
						</div>
					</div>
				</section>

				<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
				<div class="scroll-top page-scroll visible-xs visible-sm">
					<a class="btn btn-primary" href="#page-top">
						<i class="fa fa-chevron-up"></i>
					</a>
				</div>

				<!-- Portfolio Modals -->
				<div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-content">
						<div class="close-modal" data-dismiss="modal">
							<div class="lr">
								<div class="rl">
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-lg-10 col-lg-offset-1">
									<div class="modal-body">
										<section class="section_Modal">

											<h2>Dessi' Ass</h2>

											<div class="col-lg-4">
											</div>
											<div class="col-lg-4">
											</div>
											<div class="col-lg-4">
											</div>
											
											<div class="col-lg-12">
												<p>Le projet consistait à créer une logiciel de dessins assisté simplifié. Pour ce projet, seul le langage Java était autorisé. Le logiciel prend en compte uniquement les formes simples. Première partie du sujet : 	<a href="fichiers/1AJAVA12013_14.pdf" target="_blank"> Sujet Java I 2013-2014 </a> et la seconde partie  :<a href="fichiers/1AJAVA22013_14.pdf" target="_blank"> Sujet Java II 2013-2014 </a>
												</p>
											</div>
											
											<div class="col-lg-12">
												<p>
													Langage utilisé : <span class="uses">JAVA</span>
													<br/>
													Méthode utilisée : <span class="uses">SWING</span>
													<br/>
													Date : <span class="uses">Avril 2013 / Juin 2013</span>
												</p>
											</div>
										</section>
										
										<div class="col-lg-12">
											<button type="button" class="btn btn-default" data-dismiss="modal"> &nbsp;&nbsp;  Fermer &nbsp;&nbsp;  </button>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-content">
						<div class="close-modal" data-dismiss="modal">
							<div class="lr">
								<div class="rl">
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-lg-8 col-lg-offset-2">
									<div class="modal-body">
										<section class="section_Modal">
											<h2>Gestion Factures AEDI : MVC et agilité</h2>

											<div class="col-lg-4">
												<img alt="AEDI" src="images/AEDI1.png" class="img-responsive" style="float:left; margin:5px;"/>
											</div>
											<div class="col-lg-4">
												<img alt="AEDI" src="images/AEDI2.png" class="img-responsive" style="float:left; margin:5px;"/>
											</div>
											<div class="col-lg-4">
												<img alt="AEDI" src="images/AEDI3.png" class="img-responsive" style="float:left; margin:5px;"/>
											</div>
											
											<div class="col-lg-12">
												<p>Le second projet Java est effectué lors de la seconde année du DUT, les méthodes ayant évoluées il est nécessaire de mettre en pratiques ces nouvelles connaissances (MVC, DAOFactory, ...). Pour ce faire, l'équipe du département c'est mobilisé pour nous trouver un sujet à la hauteur : un logiciel de gestion pour l'association des étudiant informatique. En effet, l'association ne disposait pas d'un logiciel de gestion de facture. Nous en avons donc réalisé un.	
												</p>
											</div>
											
											<div class="col-lg-12">
												<p>
													Langage utilisé : <span class="uses">JAVA</span>
													<br/>
													Méthodes utilisées : <span class="uses">Data Access Object, SWING, JUnit, Modèle-Vue-Contrôleur. Méthode agile.</span>
													<br/>
													Date : <span class="uses">Setembre 2014 / Novembre 2014</span>
												</p>
											</div>
											
											<div class="col-lg-12">
												<p>Une petite présentation du projet est disponible en .pdf ici : <a href="fichiers/presjava2014_15.pdf" target="_blank"> Présentation AEDI </a>
												</p>
											</div>
										</section>
										
										<div class="col-lg-12">
											<button type="button" class="btn btn-default" data-dismiss="modal"> &nbsp;&nbsp;  Fermer &nbsp;&nbsp;  </button>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- jQuery -->
				<script src="js/jquery.js"></script>

				<!-- Bootstrap Core JavaScript -->
				<script src="js/bootstrap.min.js"></script>

				<!-- Plugin JavaScript -->
				<script src="httpS://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
				<script src="js/classie.js"></script>
				<script src="js/cbpAnimatedHeader.js"></script>

				<!-- Contact Form JavaScript -->
				<script src="js/jqBootstrapValidation.js"></script>
				<script src="js/contact_me.js"></script>

				<!-- Custom Theme JavaScript -->
				<script src="js/freelancer.js"></script>

			</div>
			
		</div>
		
	</div>
	
</ul> 

<style>
	.leg{
	color : black;
	background-color: #ffffff;
    border: 1px solid black;
    opacity: 0.8;
	margin-left:15px;
	font-size:16px;
	}
</style>