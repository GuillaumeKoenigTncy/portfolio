﻿<style>
	@media screen and (max-width: 480px) {
	  .divNCM {
		display: none;
	  }
	}
		
	@media screen and (min-width: 480px) {
	  .divNCP {
		display: none;
	  }
	}
		
	@media screen and (max-width: 480px) {
	  .divOCM {
		display: inline;
	  }
	}
		
	@media screen and (min-width: 480px) {
	  .divOCP {
		display: inline;
	  }
	}	
</style>

<ul class="col-sm-9">

	<div class="clearfix">

		<h2 class="Start">Autres</h2>

		<div class="col-sm-12" style="background-color:#004050;color:white;font-size:14px;text-align:justify;">
		
			<div id="page-top" class="index">
				<!-- Portfolio Grid Section -->
				<section id="portfolio">
					<div class="col-lg-12 text-center">
						<h2>&nbsp;</h2>
					</div>
					<div class="row">
						<div class="col-sm-4 portfolio-item">
							<a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
								<div class="caption">
									<div class="caption-content">
										<i class="fa fa-search-plus fa-3x"></i>
									</div>
								</div>
								<img src="images/assem1.png" class="img-responsive" alt="ASM">
								<span class="leg label bottom" ><b>Spy Game ASM</b></span> 
							</a>
						</div>
					</div>
				</section>

				<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
				<div class="scroll-top page-scroll visible-xs visible-sm">
					<a class="btn btn-primary" href="#page-top">
						<i class="fa fa-chevron-up"></i>
					</a>
				</div>

				<!-- Portfolio Modals -->
				<div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-content">
						<div class="close-modal" data-dismiss="modal">
							<div class="lr">
								<div class="rl">
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-lg-10 col-lg-offset-1">
									<div class="modal-body">
										<section class="section_Modal">

											<h2>Assembleur</h2>

											<div class="col-lg-4">
												<img alt="My CMS" src="images/assem1.png" class="img-responsive" style="float:left; margin:5px;"/>
											</div>
											<div class="col-lg-4">
												<img alt="My CMS" src="images/assem2.png" class="img-responsive" style="float:left; margin:5px;"/>
											</div>
											<div class="col-lg-4">
												<img alt="My CMS" src="images/assem3.png" class="img-responsive" style="float:left; margin:5px;"/>
											</div>

											<div class="col-lg-12">
												<p>En première année nous avons du réaliser un "jeu" en assembleur. Il n'est pas disponible au téléchargement en raison du format de fichiers. Voici le sujet : <a href="fichiers/projetasm2013_14.pdf" target="_blank"> Sujet Assembleur 2013 - 2014 </a>
												</p>	
											</div>
											
											<div class="col-lg-12">
												<p>
													Langages utilisés : <span class="uses">Assembleur</span>
													<br/>
													Méthodes utilisées : <span class="uses"> / </span>
													<br/>
													Date : <span class="uses">Mai et Juin 2013</span>
												</p>
											</div>
										</section>
										
										<div class="col-lg-12">
											<button type="button" class="btn btn-default" data-dismiss="modal"> &nbsp;&nbsp;  Fermer &nbsp;&nbsp;  </button>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- jQuery -->
				<script src="js/jquery.js"></script>

				<!-- Bootstrap Core JavaScript -->
				<script src="js/bootstrap.min.js"></script>

				<!-- Plugin JavaScript -->
				<script src="httpS://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
				<script src="js/classie.js"></script>
				<script src="js/cbpAnimatedHeader.js"></script>

				<!-- Contact Form JavaScript -->
				<script src="js/jqBootstrapValidation.js"></script>
				<script src="js/contact_me.js"></script>

				<!-- Custom Theme JavaScript -->
				<script src="js/freelancer.js"></script>

			</div>
			
		</div>
		
	</div>
	
</ul> 

<style>
	.leg{
	color : black;
	background-color: #ffffff;
    border: 1px solid black;
    opacity: 0.8;
	margin-left:15px;
	font-size:16px;
	}
</style>