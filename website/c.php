﻿<style>
	@media screen and (max-width: 480px) {
	  .divNCM {
		display: none;
	  }
	}
		
	@media screen and (min-width: 480px) {
	  .divNCP {
		display: none;
	  }
	}
		
	@media screen and (max-width: 480px) {
	  .divOCM {
		display: inline;
	  }
	}
		
	@media screen and (min-width: 480px) {
	  .divOCP {
		display: inline;
	  }
	}	
</style>

<ul class="col-sm-9">

	<div class="clearfix">

		<h2 class="Start">C</h2>

		<div class="col-sm-12" style="background-color:#004050;color:white;font-size:14px;text-align:justify;">
		
			<div id="page-top" class="index">
				<!-- Portfolio Grid Section -->
				<section id="portfolio">
					<div class="col-lg-12 text-center">
						<h2>&nbsp;</h2>
					</div>
					<div class="row">
						<div class="col-sm-4 portfolio-item">
							<a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
								<div class="caption">
									<div class="caption-content">
										<i class="fa fa-search-plus fa-3x"></i>
									</div>
								</div>
								<img src="images/nim03.png" class="img-responsive" alt="nim">
								<span class="leg label bottom" ><b>Jeu de nim</b></span> 
							</a>
						</div>
						<div class="col-sm-4 portfolio-item">
							<a href="#portfolioModal2" class="portfolio-link" data-toggle="modal">
								<div class="caption">
									<div class="caption-content">
										<i class="fa fa-search-plus fa-3x"></i>
									</div>
								</div>
								<img src="images/term.png" class="img-responsive" alt="UNIX">
								<span class="leg label bottom" ><b>Terminal UNIX</b></span> 
							</a>
						</div>
					</div>
				</section>

				<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
				<div class="scroll-top page-scroll visible-xs visible-sm">
					<a class="btn btn-primary" href="#page-top">
						<i class="fa fa-chevron-up"></i>
					</a>
				</div>

				<!-- Portfolio Modals -->
				<div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-content">
						<div class="close-modal" data-dismiss="modal">
							<div class="lr">
								<div class="rl">
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-lg-10 col-lg-offset-1">
									<div class="modal-body">
										<section class="section_Modal">

											<h2>Jeu de Nim</h2>

											<div class="col-lg-4">
												<img alt="My CMS" src="images/nim01.png" class="img-responsive" style="float:left; margin:5px;"/>
											</div>
											<div class="col-lg-4">
												<img alt="My CMS" src="images/nim02.png" class="img-responsive" style="float:left; margin:5px;"/>
											</div>
											<div class="col-lg-4">
												<img alt="My CMS" src="images/nim03.png" class="img-responsive" style="float:left; margin:5px;"/>
											</div>

											<div class="col-lg-12">
												<p>En première année nous avons du réaliser un jeu de nim en lanague c. Ce programme c s'éxécutait seulement sur la console.
													<a href="fichiers/projetc2013_14.pdf" target="_blank"> Sujet C 2013 - 2014 </a>
												</p>
											</div>

											<div class="col-lg-12">
												<p>
													Langages utilisés : <span class="uses">C</span>
													<br/>
													Méthodes utilisées : <span class="uses"> / </span>
													<br/>
													Date : <span class="uses">Octobre et Novembre 2013</span>
												</p>	 
											</div>

										</section>
										
										<div class="col-lg-12">
											<button type="button" class="btn btn-default" data-dismiss="modal"> &nbsp;&nbsp;  Fermer &nbsp;&nbsp;  </button>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-content">
						<div class="close-modal" data-dismiss="modal">
							<div class="lr">
								<div class="rl">
								</div>
							</div>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-lg-8 col-lg-offset-2">
									<div class="modal-body">
										<section class="section_Modal">

											<h2>Emulation d'un terminal UNIX</h2>

											<div class="col-lg-4">
											</div>
											<div class="col-lg-4">
												<img alt="My CMS" src="images/term.png" class="img-responsive" style="float:left; margin:5px;"/>
											</div>
											<div class="col-lg-4">
											</div>

											<div class="col-lg-12">
												<p>Le but du projet est de développer en C un terminal. Il permet d’effectuer des commandes de base comme ls, cp, mv, cat, etc. Ces différentes commandes doivent être éxécuté depuis un programme principales (père) et appelant des programmes secondaires. Ainsi le programme principale créera des "fils" qui éxécuteront les commandes tapées par l'utilisateur. Ce terminal effectue aussi une gestion des erreurs et des commandes existantes. Il est possible d'effectuer 200 commandes avec ce terminal. En plus d'effectuer les commandes de base, il permet aussi de gérer la redirection de flux.
												</p>
											</div>

											<div class="col-lg-12">
												<p>
													Langages utilisés : <span class="uses">C</span>
													<br/>
													Méthodes utilisées : <span class="uses"> / </span>
													<br/>
													Date : <span class="uses">Novembre 2014 / Janvier 2015</span>
												</p>
											</div>

											<div class="col-lg-12">
												<p>La liste des commandes disponibles ici : <a href="fichiers/listecmdc.pdf" target="_blank"> Comandes C </a></p>
											</div>
										</section>
										
										<div class="col-lg-12">
											<button type="button" class="btn btn-default" data-dismiss="modal"> &nbsp;&nbsp;  Fermer &nbsp;&nbsp;  </button>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- jQuery -->
				<script src="js/jquery.js"></script>

				<!-- Bootstrap Core JavaScript -->
				<script src="js/bootstrap.min.js"></script>

				<!-- Plugin JavaScript -->
				<script src="httpS://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
				<script src="js/classie.js"></script>
				<script src="js/cbpAnimatedHeader.js"></script>

				<!-- Contact Form JavaScript -->
				<script src="js/jqBootstrapValidation.js"></script>
				<script src="js/contact_me.js"></script>

				<!-- Custom Theme JavaScript -->
				<script src="js/freelancer.js"></script>

			</div>
			
		</div>
		
	</div>
	
</ul> 

<style>
	.leg{
	color : black;
	background-color: #ffffff;
    border: 1px solid black;
    opacity: 0.8;
	margin-left:15px;
	font-size:16px;
	}
</style>