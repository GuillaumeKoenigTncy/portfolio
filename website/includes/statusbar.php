<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="js/general.js"></script>

<style>

a.info {
   position: relative;
   color: black;
   text-decoration: none;
}
a.info span {
   display: none; /* On masque l'infobulle. */
}
a.info:hover {

   background: none; /* Correction d'un bug d'Internet Explorer. */
   z-index: 500; /* On définit une valeur pour l'ordre d'affichage. */

   cursor: help; /* On change le curseur par défaut par un curseur d'aide. */
}
a.info:hover span {
	font-size: 11px !important; /* On définit les propriétés de texte pour toutes les balises. */
   font-family: Tahoma, Verdana, Arial, serif;
	
   display: inline; /* On affiche l'infobulle. */
   position: absolute;

   white-space: nowrap; /* On change la valeur de la propriété white-space pour qu'il n'y ait pas de retour à la ligne non désiré. */

   top: -20px; /* On positionne notre infobulle. */
   left: 10px;

   background: white;

   color: #004050;
   padding: 3px;

   border: 1px solid #004050;
   border-left: 4px solid #004050;
}

</style> 

<a class="info" href="index.php" style="margin:15px 30px 0 0"><i class="fa fa-windows fa-2x"><span>Retour au menu</span></i></a>
<a class="info" target="_blank" href="fichiers/CV_V2.pdf" style="margin:15px 10px"><i class="fa fa-file-pdf-o fa-2x"><span>Vers mon CV</span></i></a>
<a class="info" target="_blank" href="https://www.facebook.com/KoenigGuillaume57" style="margin:15px 10px"><i class="fa fa-facebook-square fa-2x"><span>Vers facebook</span></i></a>
<a class="info" target="_blank" href="https://twitter.com/GKoenigTnCy" style="margin:15px 10px"><i class="fa fa-twitter-square fa-2x"><span>Vers twitter</span></i></a>