<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Portfolio Guillaume KOENIG" />
<meta name="author" content="KOENIG Guillaume" />

<title>KOENIG Guillaume - Portfolio</title>

<link rel="icon" type="image/png" href="favicon.png" />

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<!-- Bootstrap core CSS -->
<link href="bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link rel="stylesheet" type="text/css" href="css/demo-styles.css" />
<link rel="stylesheet" type="text/css" href="css/freelancer.css">
<link rel="stylesheet" type="text/css" href="css/custom.css"/>

<script>
    window.addEventListener('error', function(e) {
        document.location.href="https://oldportfolio.koenigguillaume.me";
    });
</script>